# Running the Meteor app

Assuming you have [the latest Meteor release installed][meteor-install], all
you need to do to run the app is:

    meteor run --settings settings.json

…where `settings.json` is a file like this:

    {
      "facebook" : {
        "appId" : "facebook_app_id_here",
        "secret" : "secret",
        "loginStyle": "redirect"
      }
    }

The `--settings` option allows you to add deployment-specific config variables
into the [Meteor.settings][] object.

## Initial app deployment

For some as-yet unknown reason, the first time you use `meteor run` on a fresh repository clone it will fail with an error about the Ionic framework. If you simply re-enter the `meteor run` command it will work. The reason for this remains a mystery.

# Deploying the Meteor app

The app is deployed via the `meteor build` command which produces a tarball, which is then fed into the [Ansible deploy playbook][] — read the docs of that repository for further info.

[meteor-install]: https://www.meteor.com/install
[Meteor.settings]: http://docs.meteor.com/#/full/meteor_settings
[Ansible deploy playbook]: https://bitbucket.org/malaysialive/ansible-mylive
