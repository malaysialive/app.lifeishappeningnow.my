Router.configure({
  trackPageView: true // See: https://atmospherejs.com/reywood/iron-router-ga
});

Router.route('/', {
  name: 'now'
});

Router.route('/next', {
  name: 'next'
});

Router.route('/event/:_id', {
  name: 'event.show'
});

Router.route('/users/:_id', {
  name: 'users.show'
});

Router.route('/trending', {
  name: 'trending'
});

Router.route('/profile', {
  name: 'profile'
});
