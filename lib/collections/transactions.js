Transactions = new Mongo.Collection('transactions');

Transactions.before.insert(function (userId, doc) {
  doc.createdAt = new Date();
});

Transactions.attachSchema(new SimpleSchema({
  userId: {
    type: String,
    optional: false,
    autoValue: function () {
      if (this.isSet) {
        return;
      }
      if (this.isInsert) {
        return Meteor.userId();
      } else {
        this.unset();
      }
    }
  },
  eventId: {
    type: String,
    optional: false
  },
  amount: {
    type: Number,
    decimal: true,
    min: 0,
    optional: false
  },
  currency: {
    type: String,
    optional: false,
    defaultValue: "MYR"
  },
  status: {
    type: String,
    optional: false,
    defaultValue: "Pending"
  },
  gatewayResponse: {
    type: String
  },
  createdAt: {
    type: Date
  }
}));
