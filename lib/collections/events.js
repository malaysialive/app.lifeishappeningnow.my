Events = new Mongo.Collection('events');

Events.before.insert(function (userId, doc) {
  doc.createdAt = new Date();
});

Events.helpers({
  eventDisplayDate: function () {
    return moment(this.eventDate).format("ddd Do");
  },
  eventDisplayDateTime: function () {
    return moment(this.eventDate).format("MMM Do @ h:mm a");
  },
  author: function () {
    return Meteor.users.findOne({_id: this.userId});
  },
  voters: function () {
    return Meteor.users.find({_id: {$in: this.voterIds}});
 },
  ticketCount: function() {
    var count = 0;
    _.each(this.tickets, function(element, index, list) {
      if (element.number) count+= element.number;
    });
    return count;
  }
});

RegExp.escape = function(s) {
  return s.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
};

Events.search = function(query) {
  if (!query) {
    return;
  }
  return Events.find({
    name: { $regex: RegExp.escape(query), $options: 'i' }
  }, {
    limit: 20
  });
};

Events.attachSchema(new SimpleSchema({
  url: {
    type: String,
    autoform: {
      'label-type': 'placeholder',
      placeholder: 'Event URL'
    },
    max: 200
  },
  name: {
    type: String,
    autoform: {
      'label-type': 'placeholder',
      placeholder: 'Event Name'
    },
    max: 200
  },
  description: {
    type: String,
    autoform: {
      'label-type': 'placeholder',
      placeholder: 'Description'
    },
    max: 5000
  },
  userId: {
    type: String,
    autoValue: function () {
      if (this.isSet) {
        return;
      }
      if (this.isInsert) {
        return Meteor.userId();
      } else {
        this.unset();
      }
    }
  },
  voterIds: {
    type: [String],
    optional: true,
    defaultValue: []
  },
  numberOfVotes: {
    type: Number,
    optional: true,
    defaultValue: 0
  },
  numberOfComments: {
    type: Number,
    optional: true,
    defaultValue: 0
  },
  eventDate: {
    type: Date,
    optional: false,
    defaultValue: 0
  },
  heroImage: {
    type: String,
    optional: true
  },
  venue: {
    type: String,
    optional: false
  },
  tickets: {
    type: [Object],
    optional: true
  },
  telephoneBookingNumber: {
    type: String,
    optional: true
  },
  createdAt: {
    type: Date
  }
}));
