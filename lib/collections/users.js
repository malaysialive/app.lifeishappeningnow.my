Meteor.users.before.insert(function (userId, doc) {
  doc.profile.votedEventIds = [];
});

Meteor.users.helpers({
  votedEvents: function () {
    return Events.find({_id: {$in: this.profile.votedEventIds}});
  }
});
