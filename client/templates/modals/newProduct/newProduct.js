AutoForm.hooks({
  'events-new-form': {
    onSuccess: function (operation, result, template) {
      IonModal.close();
      IonKeyboard.close();
      Router.go('event.show', {_id: result});
    }
  }
});
