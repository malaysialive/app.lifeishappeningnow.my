Template.appLayout.rendered = function () {
  Session.set('currentTab', 'now');
};

Template.appLayout.events({
  'click [data-action=share-event]': function (event, template) {
    IonActionSheet.show({
      titleText: 'Share Event (disabled for Private Beta)',
      buttons: [
        { text: '<i class="icon ion-social-twitter"></i> Twitter' },
        { text: '<i class="icon ion-social-facebook"></i> Facebook' },
        { text: '<i class="icon ion-ios-email"></i> Email' }
      ],
      cancelText: 'Cancel',
      buttonClicked: function(index) {
        if (index === 0) {
          console.log('Tweet!');
        }
        if (index === 1) {
          console.log('Facebook!');
        }
        if (index === 2) {
          console.log('Email!');
          Meteor.call('sendEmail',
            'henry@hjst.org', 'discover@lifeishappeningnow.my',
            'Here be subjects!',
            'This is the body of the email. It is unclear how attachments factor in as yet.'
          );
        }
        return true;
      }
    });
  }
});
