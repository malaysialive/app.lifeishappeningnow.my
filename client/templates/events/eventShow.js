Template.eventShow.created = function () {
  this.autorun(function () {
    this.subscription = Meteor.subscribe('event', Router.current().params._id);
  }.bind(this));
};

Template.eventShow.rendered = function () {
  this.autorun(function () {
    if (!this.subscription.ready()) {
      IonLoading.show();
    } else {
      IonLoading.hide();
    }
  }.bind(this));
};

Template.eventShow.helpers({
  event: function () {
    return Events.findOne({_id: Router.current().params._id});
  },
  ticketsLabel: function(ticketsCount) {
    if (ticketsCount > 0) {
      return " — " + ticketsCount + " left!";
    } else {
      return "";
    }
  },
  comments: function () {
    return Comments.find({
      eventId: Router.current().params._id}, {sort: {createdAt: -1}
    });
  }
});

Template.eventShow.events({
  'click [data-action=new-comment]': function (event, template) {
    if (Meteor.user()) {
      IonModal.open('newComment', {eventId: this._id});
    } else {
      IonModal.open('signIn');
    }
  },
  'click [data-action=buy-tickets]': function (event, template) {
    if (Meteor.user()) {
      IonModal.open('buyTickets', { eventId: this._id });
    } else {
      IonModal.open('signIn');
    }
  },
  'click [data-action=telephone-call]': function (event, template) {
    window.location = "tel:" + $(event.currentTarget).data('telephone-number');
  }
});
