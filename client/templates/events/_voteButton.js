Template._voteButton.events({
  'click': function (event, template) {
    event.preventDefault();

    if (!Meteor.user()) {
      IonModal.open('signIn');
      return;
    }

    Meteor.call('Events.vote', this._id);
  }
});

Template._voteButton.helpers({
  hasVotedClass: function () {
    if (!Meteor.user()) {
      return;
    }
    if(_(Meteor.user().profile.votedEventIds).contains(this._id)) {
      return 'has-voted';
    }
  }
});
