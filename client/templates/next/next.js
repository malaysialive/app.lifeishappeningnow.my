Template.next.created = function () {
  this.autorun(function () {
    this.subscription = Meteor.subscribe('events');
  }.bind(this));
};

Template.next.rendered = function () {
  this.autorun(function () {
    if (!this.subscription.ready()) {
      IonLoading.show();
    } else {
      IonLoading.hide();
    }
  }.bind(this));
};

Template.next.helpers({
  events: function () {
    return Events.find({}, { sort: { eventDate: -1 }});
  }
});
