Template.now.created = function () {
  this.autorun(function () {
    this.subscription = Meteor.subscribe('events');
  }.bind(this));
};

Template.now.rendered = function () {
  this.autorun(function () {
    if (!this.subscription.ready()) {
      IonLoading.show();
    } else {
      IonLoading.hide();
    }
  }.bind(this));
};

Template.now.helpers({
  events: function () {
    /*
      Return a list of "Now" events. This is adaptive, so if there is a
      sufficient number of events in the future (≥ $threshold) then events are
      shown in ascending order from $now. In addition, $now is calculated as
      $duration hours in the past, to allow for showing events currently in
      progress.

      If there is an insufficient number of events then they are displayed in
      descending date order to "pad the numbers" while we're in beta and don't
      always have enough events in a given period
     */
    var threshold = 5;
    var duration = 4;
    var now = new Date(
      new Date().setHours( new Date().getHours() - duration )
    );
    var events = Events.find({
      eventDate: { $gte: now }
    }, {
      sort: { eventDate: 1 },
      limit: 7
    });

    if (events.count() < threshold) {
      // we have an insufficient number of events in the future to fill the
      // "Now" tab
      events = Events.find({}, {
        sort: { eventDate: -1 },
        limit: 9
      });
    }
    return events;
  }
});
