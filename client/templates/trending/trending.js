Template.trending.created = function () {
  this.autorun(function () {
    this.subscription = Meteor.subscribe('events');
  }.bind(this));
};

Template.trending.rendered = function () {
  this.autorun(function () {
    if (!this.subscription.ready()) {
      IonLoading.show();
    } else {
      IonLoading.hide();
    }
  }.bind(this));
};

Template.trending.helpers({
  events: function () {
    /*
     Return a list of "Trending" events. This is adaptive, so if there is a
     sufficient number of events in the future (≥ $threshold) then events are
     shown ranked by number of votes in descending order.

     If there is an insufficient number of events then they are displayed in
     descending date order from the last $period days to "pad the numbers" while
     we're in beta and don't always have enough events.
     */
    var threshold = 7;
    var period = 21;
    var now = new Date();
    var events = Events.find({
      eventDate: {$gte: now},
      numberOfVotes: {$gt: 0}
    }, {
      sort: { numberOfVotes: -1, eventDate: -1 }
    });

    if (events.count() < threshold) {
      // we have an insufficient number of events in the future to fill the
      // "Trending" tab.
      var oldestEventCutoff = new Date(
          //
          new Date().setDate( new Date().getDate() - period )
      );
      events = Events.find({
        numberOfVotes: {$gt: 0},
        eventDate: {$gte: oldestEventCutoff}
      }, {
        sort: { numberOfVotes: -1, eventDate: -1}
      });
    }
    return events;
  }
});