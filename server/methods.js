Meteor.methods({
  'Events.vote': function (_id) {
    if (!Meteor.user()) {
      return;
    }

    if (_(Meteor.user().profile.votedEventIds).include(_id)) {
      return;
    }

    Events.update({ _id: _id }, {
      $inc: {
        numberOfVotes: 1
      },
      $addToSet: {
        voterIds: this.userId
      }
    });

    Meteor.users.update({ _id: this.userId }, {
      $addToSet: {
        'profile.votedEventIds': _id
      }
    });
  },
  'sendEmail': function(to, from, subject, text) {
    check([ to, from, subject, text ], [String]);
    // Let other method calls from the same client start running, without
    // waiting for the email sending to complete.
    this.unblock();
    Email.send({
      to: to,
      from: from,
      subject: subject,
      text: text
    });
  },
  'sendSlack': function(botname, icon, channel, message) {
    var options = { data: {
      channel: channel,
      username: botname,
      icon_emoji: ":"+ icon +":",
      text: message
    }};
    HTTP.call(
        "POST",
        "https://hooks.slack.com/services/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",
        options
    );
  }
});
