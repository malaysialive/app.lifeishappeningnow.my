Meteor.startup(function() {
  var users = [
    {
      emails: [{
        address: "henry@lifeishappeningnow.my",
        verified: true,
        primary: true
      }],
      profile: {
        name: "Henry Todd"
      },
      services: {
        "meteor-developer": {
          id: "AqoSNxjZAXNRd5YFK",
          username: "hjst",
          emails: [{
            address: "henry@lifeishappeningnow.my",
            verified: true,
            primary: true
          }]
        }
      }
    }
  ];

  var events = [
    {
      url: "https://www.facebook.com/events/358681567673432/",
      name: "Az Samad Songwriter Playground",
      eventDate: new Date("2015-03-28T14:00"),
      heroImage: "",
      description: "Not your average playground, support our local musicians as they swing and slide down their guitar frets at Songwriters Playground, curated by Az Samad, this weekend. Unlike other shows, featured musicians Az Samad, Reza Salleh and Anna Chong will take turns to showcase their latest materials in which we’re expecting a fun banter in between! Just an idea, let’s make things interesting and throw in a random topic for them to perform on stage. Well, maybe not.",
      venue: "Alexis, Great Eastern Mall, Kuala Lumpur"
    },
    {
      url: "https://www.facebook.com/pages/Projek-SeniKami/752945618092775?fref=ts",
      name: "Projek SeniKami Workshop",
      eventDate: new Date("2015-03-28T02:00"),
      heroImage: "",
      description: "Skip the typical bustling weekend and release your inner creativity with Projek SeniKami for their creative art workshop with the Orang Asli children all the way in Kampung Pisang Rasau! The peeps of SeniKami have decided that this week’s theme is ‘Bookmaking and Doodling’; so do you have what it takes to create the most artistic and interesting masterpieces in one day? Join Projek SeniKami for a day trip this Saturday to find out!",
      venue: "Kampung Pisang Rasau, Slim River Perak"
    },
    {
      url: "https://www.facebook.com/events/1542716726002709/",
      name: "Bukan Sekadar Bicara : Perempuan dan Filem",
      eventDate: new Date("2015-03-27T12:30"),
      heroImage: "",
      description: "What does it mean to be a leading lady of Malaysia in today's society? Sit down and learn a thing or two with the successful feline insiders of the Malaysian film industry about the correlation between women and film only at Bukan Sekadar Bicara: Perempuan dan Filem at Gerak Budaya, PJ this Saturday.",
      venue: "Gerak Budaya, Petaling Jaya"
    },
    {
      url: "https://www.facebook.com/pages/Projek-SeniKami/752945618092775?fref=ts",
      name: "Projek SeniKami Gig",
      eventDate: new Date("2015-03-29T07:00"),
      heroImage: "https://s3-ap-southeast-1.amazonaws.com/app.lifeishappeningnow.my/events/11121146_969301473080118_1533274630_n.jpg",
      description: "Help the children of Malaysia to be more creative by spending your Sunday afternoon with on-the-spot drawing, arts, and live performances with Projek SeniKami at Kedai Sebelah, Kelana Jaya. You can take part in this fundraising event by donating a minimum donation of RM5 or an art material of equal value (colour pens, pencils, brushes etc) to help purchase art and creative learning materials for the children in need!",
      venue: "Kedai Sebelah, Kelana Jaya"
    },
    {
      url: "https://www.facebook.com/events/1522978744643949/",
      name: "RANTAI Arts Fest",
      eventDate: new Date("2015-03-28T02:00"),
      heroImage: "",
      description: "The yearly most acclaimed art festival, Rantai Art, returns to the heart of Kuala Lumpur this weekend as a platform for local artists to show off their strange, quirky and amazing masterpieces to the public. From creative workshops t o street performances (and hopefully a truckload of food stalls), you’re definitely in for an artsy-fartsy treat this weekend!",
      venue: "MaTiC (Malaysia Tourism Centre)"
    },
    {
      url: "https://www.facebook.com/events/1429064897393430/",
      name: "Live @ Kedai Sebelah",
      eventDate: new Date("2015-03-31T12:30"),
      heroImage: "https://s3-ap-southeast-1.amazonaws.com/app.lifeishappeningnow.my/events/11120065_969301489746783_1321248575_n.jpg",
      description: "Who says Malaysians have no talented musicians? Spend your Tuesday night in Kedai Sebelah tonight to be serenaded by our local singer-songwriters Paolo Delfino, Bambino Kiss, Black & Blues and Zalila Lee. Best when paired with Nasi Goreng Embah and Teh Botol.",
      venue: "Kedai Sebelah Kelana Jaya"
    },
    {
      url: "https://www.facebook.com/events/961314607212323/",
      name: "Loong & Nicolai (UK) live",
      eventDate: new Date("2015-03-31T12:30"),
      heroImage: "",
      description: "For one night only, Merdekarya presents a special evening with Loong & Nicolai as they share their experiences from their adventures around the world through music and lyrics. If you think you have a better story than the boys, do step on the stage and register for the open mic session. Don’t worry, art is subjective.",
      venue: "Merdekarya PJ"
    },
    {
      url: "https://www.facebook.com/events/1606368409576045/",
      name: "Pastel Lite Live",
      eventDate: new Date("2015-04-01T13:30"),
      heroImage: "https://s3-ap-southeast-1.amazonaws.com/app.lifeishappeningnow.my/events/11063207_969302543080011_1007424814_n.jpg",
      description: "Experimental duo Eff Hakim and Mohd Faliq a.k.a. Pastel Lite is set to take the center stage of Pisco this Wednesday. The “stage” might be a little bit smaller than the one in Laneway Singapore 2015 (yep, Pastel Lite was one of the acts!), but trust them to take you on another level of electrifying goodness like no other. Since it’s ladies night, try your luck at the bar to get yourself a free drink or two. No harm in trying but don't blame us if it doesn't work though!",
      venue: "Pisco Bar, Changkat KL"
    },
    {
      url: "https://www.facebook.com/TheGrumpyCyclist",
      name: "The Grumpy Cyclist Night Ride",
      eventDate: new Date("2015-04-02T12:30"),
      heroImage: "https://s3-ap-southeast-1.amazonaws.com/app.lifeishappeningnow.my/events/11079806_969301483080117_1088442360_n.jpg",
      description: "Another Thursday, another Grumpy Cyclist Night Ride! Polish your bikes, tighten your helmets and make your way to The Grumpy Cyclist in TTDI this Thursday to embark on a magical journey (with the help of caffeine) through the concrete jungle we call Kuala Lumpur. If that’s hard to imagine, then picture yourself in New York City because ‘Bicycles’ + ‘coffee’ + ‘city’ macam NYC lah!",
      venue: "TTDI"
    },
    {
      url: "https://www.facebook.com/events/1606362556243910/",
      name: "Azmyl Yunor Live",
      eventDate: new Date("2015-04-02T12:30"),
      heroImage: "https://s3-ap-southeast-1.amazonaws.com/app.lifeishappeningnow.my/events/11128185_969302546413344_1522504730_n.jpg",
      description: "Thursday nights in April will never be dull again as Azmyl Yunor strums his tunes and belts his inspiring words of wisdom in the home of local artistes, Merdekarya.",
      venue: "Merdekarya, PJ"
    },
    {
      url: "https://www.facebook.com/events/1606368409576045/",
      name: "KYSERUN KREW Night Run",
      eventDate: new Date("2015-04-03T12:30"),
      heroImage: "https://s3-ap-southeast-1.amazonaws.com/app.lifeishappeningnow.my/events/11106522_969302556413343_1682753004_n.jpg",
      description: "Release your inner Forrest Gump, show off your lanky legs, and run with KYSERUN KREW this Friday to keep you fit for the weekend parties!",
      venue: "MPSJ Subang"
    },
    {
      url: "https://www.facebook.com/MalaysianInvasion",
      name: "Malaysia Invasion Mixed Martial Arts (MIMMA) Try Out's",
      eventDate: new Date("2015-04-25T14:30"),
      heroImage: "https://s3-ap-southeast-1.amazonaws.com/app.lifeishappeningnow.my/events/MIMMA.jpg",
      heroVideo: "https://s3-ap-southeast-1.amazonaws.com/app.lifeishappeningnow.my/events/mimma_vine_clip.mp4",
      heroVideoPoster: "https://s3-ap-southeast-1.amazonaws.com/app.lifeishappeningnow.my/events/mimma_vine_clip.mp4.jpg",
      description: "Think you're one tough cookie? Show off your strong Penang taiko skills and take down your opponents (without getting arrested woop!) at the MIMMA tryouts this weekend.",
      venue: "Prangin Mall, Penang"
    },
    {
      url: "https://www.facebook.com/events/867640419948548/",
      name: "How to: Live your life according to someone else",
      eventDate: new Date("2015-04-04T12:00"),
      heroImage: "https://s3-ap-southeast-1.amazonaws.com/app.lifeishappeningnow.my/events/11103994_969301466413452_1639492154_n.jpg",
      description: "Be a part of an art exhibition by the amazing Lily Osman, Leo Ari and Nazrul Hamzah to find out how to live your life according to someone else. Quick question: if you could pick anyone’s life to live, who would you pick? Why? Comment below.",
      venue: "Kedai Sebelah"
    },
    {
      url: "http://www.lifeishappeningnow.my",
      name: "Jonesy presents: PokéMANIA",
      eventDate: new Date("2015-04-01T15:30"),
      heroImage: "https://s3-ap-southeast-1.amazonaws.com/app.lifeishappeningnow.my/events/twinkachu.jpg",
      description: "Introducing the unique talents of young men dressing as Pokémon then posing for photographs. Truly what a wonderful world   we live in. Come early to avoid disappointment.",
      venue: "Chez Jones"
    },
    {
      url: "http://www.klpac.org/",
      name: "Speed The Plow",
      eventDate: new Date("2015-04-09T12:30"),
      heroImage: "https://s3-ap-southeast-1.amazonaws.com/app.lifeishappeningnow.my/events/Photo+07-04-2015+14+24+58_600x600.jpg",
      description: "Hands-down the funniest show of this season, internationally-known short play Speed-The-Plow by David Mamet will be on stage in Malaysia for the very first time! Featuring a brilliant cast of three – Gavin Yap (actor/director), Douglas Lim (stand-up comedian) and America Henderson (actress/model/TV host), they will be tickling your funny bones in this satirical take of the American movie business and the clash between art and commerce found in corners of entertainment and communication!",
      venue: "KLPAC Sentul"
    },
    {
      url: "https://www.facebook.com/events/456702477813039/",
      name: "Kien Lim, Calling Relevance, Qierra & Le' Lagoo",
      eventDate: new Date("2015-04-08T13:30"),
      heroImage: "https://s3-ap-southeast-1.amazonaws.com/app.lifeishappeningnow.my/events/Photo+07-04-2015+14+52+16_600x600.jpg",
      description: "Is this the best threesome ladies night ever or what? Get your pretty sweet asses, we mean selves, to Pisco Bar this Wednesday for a pretty chilled night with three budding singer/songwriters Kien Lim, Calling Relevance and Qierra.",
      venue: "Pisco Bar"
    },
    {
      url: "https://www.facebook.com/kedaisebelah",
      name: "Live Singer Songwriter Showcase",
      eventDate: new Date("2015-04-15T12:30"),
      heroImage: "https://s3-ap-southeast-1.amazonaws.com/app.lifeishappeningnow.my/events/Photo+07-04-2015+15+08+17_600x600.jpg",
      description: "This is what you should be doing this Wednesday night! Hailing all the way from the land down under, Lisa Spykers joins up-and-coming local singers/songwriters Sarah and Amee Azhar at our favourite joint for yet another round of singer/songwriter showcase!",
      venue: "Kedai Sebelah"
    },
    {
      url: "https://www.facebook.com/events/801923883233609/",
      name: "Lazy Day @ Pisco Bar",
      eventDate: new Date("2015-04-19T07:00"),
      heroImage: "https://s3-ap-southeast-1.amazonaws.com/app.lifeishappeningnow.my/events/Photo+06-04-2015+09+54+34_600x600.jpg",
      description: "Cure your Saturday night hangover with yet another hangover at the laziest and most relaxed party of the month at Pisco Bar’s Lazy Day. Kick back and relax on a hammock with a drink of your choice in hand; fall into a deep slumber on the multi-coloured bean bags; or get ready to get your hands dirty as you chomp down the BBQ-ed meat while Alex ASquared, The Other Guy & Taufiq a.k.a. The Other Party spin the feel-good music for a lazy evening like no other…",
      venue: "Pisco Bar KL"
    },
    {
      url: "https://www.facebook.com/kedaisebelah",
      name: "Live @ Kedai Sebelah (Acoustic Showcase)",
      eventDate: new Date("2015-04-28T12:30"),
      heroImage: "https://s3-ap-southeast-1.amazonaws.com/app.lifeishappeningnow.my/events/Photo+26-04-2015+18+48+56+600x600.jpg",
      description: "Drink a cup of kopi and enjoy the raw, live and unedited performances by local musicians only at Kedai Sebelah!",
      venue: "Kedai Sebelah Kelana Jaya"
    },
    {
      url: "https://www.facebook.com/events/1606368409576045/",
      name: "Kyserun Krew Meet up and Run",
      eventDate: new Date("2015-04-28T12:30"),
      heroImage: "https://s3-ap-southeast-1.amazonaws.com/app.lifeishappeningnow.my/events/Photo+26-04-2015+18+43+16+600x600.jpg",
      description: "Forget the gym and run with the wind with the coolest runners in town, Kyserun Krew, in Subang.",
      venue: "Stadium Mpsj, Subang jaya"
    },
    {
      url: "https://www.facebook.com/piscobarkl",
      name: "Pisco Live with RELENT, Uncommon Creature & Joanne and Julia",
      eventDate: new Date("2015-04-29T13:30"),
      heroImage: "https://s3-ap-southeast-1.amazonaws.com/app.lifeishappeningnow.my/events/Photo+26-04-2015+18+48+55+600x600.jpg",
      description: "Drop down to the bass with RELENT, Uncommon Creature and Joanne & Julia exclusively at Pisco Bar this Wednesday! Don't miss out!",
      venue: "Pisco Bar KL"
    },
    {
      url: "https://www.facebook.com/events/820384558050234/",
      name: "HAHA Happy Birthday Crackhouse Comedy Featuring Harith Iskander",
      eventDate: new Date("2015-04-30T13:30"),
      heroImage: "https://s3-ap-southeast-1.amazonaws.com/app.lifeishappeningnow.my/events/Photo+26-04-2015+18+48+54+600x600.jpg",
      description: "Forget the laughing gas and just drop by at our favourite crackhouse to ROFL and LMAO with Malaysian Godfather of Comedy Harith Iskander this Thursday!",
      venue: "Crackhouse Comedy Club, TTDI"
    },
    {
      url: "https://www.facebook.com/TheGrumpyCyclist",
      name: "The Grumpy cyclist night ride",
      eventDate: new Date("2015-04-30T12:30"),
      heroImage: "https://s3-ap-southeast-1.amazonaws.com/app.lifeishappeningnow.my/events/Photo+26-04-2015+19+19+35+600x600.jpg",
      description: "Explore the city of dreams a.k.a. Kuala Lumpur with the nightriders of The Grumpy Cyclist this Thursday. Down a shot (or two) of espresso to get an extra boost of energy!",
      venue: "The Grumpy cyclist, TTDI"
    },
    {
      url: "https://www.facebook.com/piscobarkl",
      name: "The Panther invites you to his Play-House",
      eventDate: new Date("2015-04-30T14:30"),
      heroImage: "https://s3-ap-southeast-1.amazonaws.com/app.lifeishappeningnow.my/events/Photo+26-04-2015+19+40+10+600x600.jpg",
      description: "Whether you need it slinky like a Miami Beach party or hands-in-the-air-glory-be-this-scared-music NY diva style or duurrrty-yet-groovy-hands-all-over-your... London basement nastiness. The Panther wants to take you along on wild uninhibited ride through his substantially large...errr...house music box of goodies. Ladies just remember...There ain't no house party like a Panther's Playhouse party.",
      venue: "Pisco Bar KL"
    },
    {
      url: "http://www.mytalentlounge.com/",
      name: "REVOFEV & CASA AUDIO PRESENTS: Woodcraft Vol 3",
      eventDate: new Date("2015-05-02T09:00"),
      heroImage: "",
      description: "...And the Woodcraft series returns with the hottest and most talented local musicians under one roof only at Talent Lounge, Menara Mustafa Kamal featuring Enterprise, SXS, JaggFuzzBeats and many more! So come down and get your mind blown with the best of the best underdogs of the Malaysian music scene.",
      venue: "Talent Lounge, Damansara Perdana"
    },
    {
      url: "https://www.facebook.com/events/1758310047728808/",
      name: "THE GALA - the Cream of the Carnival in one massively hilarious grand finale",
      eventDate: new Date("2015-05-03T13:00"),
      heroImage: "https://s3-ap-southeast-1.amazonaws.com/app.lifeishappeningnow.my/events/Photo+26-04-2015+19+29+43+600x600.jpg",
      description: "This is probably the biggest and the funniest birthday celebration that we can ever imagine! Step your foot at Crackhouse KL this Sunday for the crème de la crème of a hilariously-packed week of local and international comedy that will make your Monday a little bit more bearable!",
      venue: "Crackhouse Comedy Club, TTDI",
      telephoneBookingNumber: "+601234567890"
    },
  ];

  if (Meteor.users.find({}).count() === 0) {
    _(users).each(function (user) {
      Meteor.users.insert(user);
    });
  }

  var author = Meteor.users.find().fetch()[0];
  if (Events.find({}).count() === 0) {
    _(events).each(function (event) {
      Events.insert({
        userId: author._id,
        url: event.url,
        name: event.name,
        eventDate: event.eventDate,
        heroImage: event.heroImage,
        description: event.description,
        venue: event.venue,
        createdAt: new Date()
      });
    });
  }
});
