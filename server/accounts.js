Meteor.startup(function() {
  ServiceConfiguration.configurations.remove({ service: 'facebook' });
  ServiceConfiguration.configurations.insert({
    service: 'facebook',
    appId: Meteor.settings.facebook.appId,
    secret: Meteor.settings.facebook.secret,
    loginStyle: Meteor.settings.facebook.loginStyle
  });
});

Accounts.onCreateUser(function(options, user) {
  //user.emails = user.services['facebook'].email;
  user.profile = options.profile;
  Meteor.call('sendSlack',
      "New User",
      "seedling",
      "#general",
      "Created an account for <"
        + user.services['facebook'].link
        + "|" + user.services['facebook'].name
        +"> ("+ user.services['facebook'].email +")"
  );
  return user;
});
