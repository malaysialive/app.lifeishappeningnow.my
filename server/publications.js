Meteor.publish('events', function() {
  return Events.find();
});

Meteor.publish('eventsSearch', function(query) {
  check(query, String);

  if (_.isEmpty(query)) {
    return this.ready();
  }

  return Events.search(query);
});

Meteor.publishComposite('event', function(_id) {
  return {
    find: function() {
      return Events.find({_id: _id});
    },
    children: [
      {
        find: function(event) {
          return Meteor.users.find({_id: event.userId});
        }
      },
      {
        find: function(event) {
          return Meteor.users.find({_id: event.voterIds});
        }
      },
      {
        find: function(event) {
          return Comments.find({eventId: event._id});
        },
        children: [
          {
            find: function(comment) {
              return Meteor.users.find({_id: comment.userId});
            }
          }
        ]
      }
    ]
  };
});

Meteor.publishComposite('user', function(_id) {
  return {
    find: function() {
      return Meteor.users.find({_id: _id});
    },
    children: [
      {
        find: function(user) {
          return Events.find({_id: {$in: user.profile.votedEventIds}});
        }
      }
    ]
  };
});
